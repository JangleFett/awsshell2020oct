#!/bin/bash -xv

yum -y install httpd
systemctl enable httpd
systemctl start httpd

cat >/var/www/html/index.html <<_END_
<html>
        <body>
                <h1>Woohoo - Your first web server!</h1>
                <img src="https://upload.wikimedia.org/wikipedia/commons/0/04/So_happy_smiling_cat.jpg">
        </body>
</html>
_END_

echo "Script completed"
