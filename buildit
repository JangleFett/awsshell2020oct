#!/bin/bash
if (( $# < 4 ))
then
	echo "SYNTAX: $0 <sshkeyname> <subnetID> <HOMEIP> <MCTYPE>" 1>&2
	exit 1
fi

if [[ -z $AWS_DEFAULT_REGION ]]
then
	echo -n "Use eu-west-1? (y/n): "
	read response

	if [[ $response == y* ]]
	then
		AWS_DEFAULT_REGION=eu-west-1
	else
		echo "Type region: "
		read AWS_DEFAULT_REGION
	fi
fi

export AWS_DEFAULT_REGION

sshkey=$1
subnetID=$2
HOMEIP=$3
MCTYPE=$4

case $MCTYPE in
	httpd|apache)	FILE=apache
								;;
	nginx)				FILE=nginx
								;;
	haproxy)			FILE=haproxy
								;;
	*)						echo "I don't understand that option"
								exit 1
								;;
esac

# Check if secgrp exists
sgexist=$(aws ec2 describe-security-groups | jq -r '.SecurityGroups[] | select(.GroupName == "StevesBuildit")')

if [[ -z $sgexist ]]
then
	# Create secgrp for personal 22 and 80 to all
	echo "Creating security group"
	secgrp=$(aws ec2 create-security-group --description "Steves Temporary Image build group" --group-name "StevesBuildit")
	groupID=$(echo "$secgrp" | jq -r '.GroupId')
	echo "Security group $groupID created"

	aws ec2 authorize-security-group-ingress --group-name "StevesBuildit" --protocol tcp --port 22 --cidr ${HOMEIP}/32
	aws ec2 authorize-security-group-ingress --group-name "StevesBuildit" --protocol tcp --port 80 --cidr 0.0.0.0/0
else
	groupID=$(aws ec2 describe-security-groups | jq -r '.SecurityGroups[] | select(.GroupName == "StevesBuildit") | .GroupId')
	echo "Using existing secgrpid $groupID"
fi

# Create instance
myvm=$(aws ec2 run-instances --image-id ami-0bb3fad3c0286ebd5 --count 1 --instance-type t2.micro --key-name $sshkey --security-group-ids $groupID --subnet-id $subnetID --user-data file://${FILE}.txt)

id=$(echo "$myvm" | jq -r '.Instances[].InstanceId')
echo "Instance ID = $id"

if ! aws ec2 create-tags --resources $id $groupID --tags Key=Name,Value=StevesBuildit >/dev/null 2>&1
then
	echo "Failed to tag instance and SecGroup!"
	exit 1
fi

dnsname=""
while [[ -z $dnsname ]]
do
	instancedetail=$(aws ec2 describe-instances --instance-ids $id)
	dnsname=$(echo "$instancedetail" | jq -r '.Reservations[].Instances[].PublicDnsName')
	sleep 5
done

echo "DNS Name for instance is $dnsname"

#Wait up to 2 minutes to see if we get a valid web page
count=0
while (( count < 12 ))
do
	if (( $(curl -s $dnsname | wc -l) > 0 ))
	then
		echo "Web server is up and running"
		echo "Instance provisioned and working"
		break
	fi
	(( count = count + 1 ))
	sleep 10
done

if (( count < 12 ))
then
	echo "Creating AMI"
	amiid=$(aws ec2 create-image --instance-id $id --name Steves${FILE}Image --description "Automated build of ${FILE} on Amazon Linux" | jq -r '.ImageId' )
	if [[ -z $amiid ]]
	then
		count=31
	else
		count=0
	fi

	while (( count < 30 ))
	do
		amistate=$(aws ec2 describe-images --image-ids $amiid | jq -r '.Images[].State')
		sleep 20
		if [[ $amistate == "available" ]]
		then
			echo "AMI $amiid created"
			break
		fi
		(( count = count + 1 ))
	done
fi

# Always clean up by deleting instance and secgrp
aws ec2 terminate-instances --instance-ids $id
# Wait for instance to terminate before deleting secgrp
while ! aws ec2 describe-instances --instance-ids $id | jq -r '.Reservations[].Instances[].State.Name' | grep terminated
do
	sleep 10
done
echo "Deleting security group $groupID"
aws ec2 delete-security-group --group-id $groupID

echo "Done"
